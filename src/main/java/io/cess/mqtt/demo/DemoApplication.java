package io.cess.mqtt.demo;

import io.cess.mqtt.demo.config.MqttConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author wcl
 * @version 1.0
 * @date 2019/12/14 5:50 下午
 */
@SpringBootApplication
@Import(MqttConfig.class)
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
